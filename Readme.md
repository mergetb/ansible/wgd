# Wireguard Dameon Ansible Role

This role installs and configures the [wgd](https://gitlab.com/mergetb/tech/wgd)
Wireguard configuarion daemon.

## Variables

| name | required | default | description |
| ---- | ---------| ------- | ----------- |
| endpoint | yes | | endpoint for wgd to listen on |
| cert | yes | | TLS cert |
| key | yes | | TLS key |

### Available platforms

- ubuntu
- debian

## Examples

```yaml
- include_role:
    name: daemon
  vars:
    endpoint: 0.0.0.0:36000
    cert: cert.pem
    key: key.pem
```
